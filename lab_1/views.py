from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = 'Muhammad Arief Darmawan' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1999,1,28) #TODO Implement this, format (Year, Month, Date)
npm = 1706043746 # TODO Implement this
tempat_kuliah = 'Universitas Indonesia'
hobi = 'Foto'
deskripsi = "Mana dimana anak kambing saya, anak kambing tuan ada di pohon waru, mana dimana jantung hati saya, jantung hati tuan ada di kampung baru" 


#Profil teman di depan
mhs_name_depan = 'Hamri Hamdika' # TODO Implement this
curr_year_depan = int(datetime.now().strftime("%Y"))
birth_date_depan = date(1996,1,22) #TODO Implement this, format (Year, Month, Date)
npm_depan = 1406601574 # TODO Implement this
tempat_kuliah_depan = 'Universitas Indonesia'
hobi_depan = 'Membaca'
deskripsi_depan = 'Mahasiswa Matrikulasi'

#Profil teman di belakang
mhs_name_belakang = 'Tembok' # TODO Implement this


# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm,
    'tempat_kuliah' : tempat_kuliah, 'hobi' : hobi, 'deskripsi' : deskripsi,
    'name_depan' : mhs_name_depan, 'age_depan' : calculate_age(birth_date_depan.year), 'npm_depan' : npm_depan,
    'tempat_kuliah_depan' : tempat_kuliah_depan, 'hobi_depan' : hobi_depan, 'deskripsi_depan' : deskripsi_depan,
    'name_belakang' : mhs_name_belakang}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
