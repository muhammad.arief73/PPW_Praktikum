
from django import forms

class Status_Form(forms.Form):
	error_messages = {
	'required': 'This field is required.',
	
	}

	attrs = {
	'class': 'form-control'
	}
	status = forms.CharField(label='status', required=True, max_length=300,
	widget=forms.TextInput(attrs=attrs))

class Regist_form(forms.Form):
	error_messages = {
	'required': 'This field is required.',
	
	}

	attrs = {
	'class': 'form-control'
	}
	nama = forms.CharField(
		label='Nama', 
		required=True, 
		max_length=100,
		widget=forms.TextInput(attrs=attrs))

	email = forms.CharField(
		label='E-Mail',
		required=True,
		max_length=100,
		widget=forms.EmailInput(attrs=attrs))

	password = forms.CharField(
		label = 'Password',
		required=True,
		max_length = 20,
		widget = forms.PasswordInput(attrs=attrs))
