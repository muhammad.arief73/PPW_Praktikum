from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import content, validateEmail
from .models import Status
from .forms import Status_Form
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
import lab6.views as views


# Create your tests here.
class Lab6UnitTest(TestCase):

	def test_hello_name_is_exist(self):
		response = Client().get('/lab-6/')
		self.assertEqual(response.status_code,200)

	#challaenge test
	def test_landing_page_is_exist(self):
		response = Client().get('/lab-6/profile/')
		self.assertEqual(response.status_code,200)

	# def test_nama_is_exist(self):
	# 	request = HttpRequest()
	# 	response = content(request)
	# 	html_response = response.content.decode('utf8')
	# 	self.assertIn(nama_mahasiswa, html_response)

	def test_hello_apa_kabar(self):
		request = HttpRequest()
		response = content(request)
		html_response = response.content.decode('utf8')
		self.assertIn('Hello, Apa kabar?', html_response)

	def test_lab6_used_html_tags_response(self):
		response = Client().get('/lab-6/')
		self.assertContains(response, '<form' , 2)

	def test_model_can_create_new_todo(self):
            # Creating a new activity
            new_activity = Status.objects.create(status='mengerjakan story ppw')
            request = HttpRequest()
            response = content(request)
            # Retrieving all available activity
            counting_all_available_todo = Status.objects.all().count()
            html_response = response.content.decode('utf8')
            self.assertEqual(counting_all_available_todo, 1)
            self.assertIn('mengerjakan story ppw', html_response )

	def test_model_delete(self):
		new_activity = Status.objects.create(status = 'hakuna matata')
		new_activity.delete()
		counting_all_available_todo = Status.objects.all().count()
		self.assertEqual(counting_all_available_todo, 0)    	


	def test_form_validation_for_blank_items(self):
		form = Status_Form(data = {'status' : ''})
		self.assertFalse(form.is_valid())
		form = Status_Form(data={'status': ''})
		self.assertFalse(form.is_valid())
		self.assertEqual(form.errors['status'],
			["This field is required."])


	# Test Lab-9
	def test_open_API(self):
		response = Client().get('/lab-6/list_books/')
		self.assertEqual(response.status_code,200)

	# def test_open_list_books(self):
	# 	response = Client().get('/lab-6/buku/')
	# 	self.assertEqual(response.status_code,200)

	# test lab 10
	def test_page(self):
		response = Client().get('/lab-6/register/')
		self.assertEqual(response.status_code,200)

	def test_validateEmail(self):
		found = resolve('/lab-6/validate/')
		self.assertEqual(found.func, views.validateEmail)

# test lab 11
	def test_startpage(self):
		response = Client().get('/lab-6/signIn/')
		self.assertEqual(response.status_code,200)

	def test_logout(self):
		response = Client().get('/lab-6/logout/')
		self.assertEqual(response.status_code,302)
	
class Lab6FunctionTest(TestCase):
	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		service_log_path = "./chromedriver.log"
		service_args = ['--verbose']
		self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		super(Lab6FunctionTest, self).setUp()

	def tearDown(self):
		self.selenium.quit()
		super(Lab6FunctionTest, self).tearDown()

	def test_input_todo(self):
		selenium = self.selenium
		# Opening the link we want to test
		selenium.get('http://localhost:8000/lab-6/')

		#Time freeze
		time.sleep(5)

		# find the form element
		status = selenium.find_element_by_id('id_status')

		submit = selenium.find_element_by_id('submit')

		# Fill the form with data
		status.send_keys('Coba Coba')

		# submitting the form
		submit.send_keys(Keys.RETURN)

		self.assertIn('Coba Coba', self.selenium.page_source)

		#Time freeze
		time.sleep(5)


	




		



