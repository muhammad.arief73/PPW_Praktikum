from django.db import models
from django.utils import timezone
from datetime import datetime, date

# Create your models here.
class Status(models.Model):
	status = models.CharField(max_length=300)
	published_date = models.DateTimeField(default=timezone.now)

# Model Lab 10
class Regist(models.Model):
	nama = models.CharField(max_length=100)
	email = models.CharField(max_length = 100)
	password = models.CharField(max_length = 20)
	