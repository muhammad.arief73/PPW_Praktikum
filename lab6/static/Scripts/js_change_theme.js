$(document).ready(function(){
    $("#btn1").click(function(){
    	$("body").css({"background-color" : "white",
    					"color" : "black"})

    	$(".accordion").css({"background-color": "#343a40", 
                             "color" : "white",
    						 "border": "1px solid white",
                             "border-radius": "0px"});

        $(".panel").css({"background-color": "white",
                         "color" : "black",
                         "text-align" : "left"});

        
        
        $(".card-title").css({"color": "black"})
        $("#status").css({"color": "black"})
        $(".card-body").css({"background-color": "white", "border-radius":"0px"})
        $(".card").css({"background-color": "white", "border-radius":"0px"})
    });
    $("#btn2").click(function(){
    	$("body").css({"background-color" : "#95a5a6",
    					"color" : "white"})
        $(".panel").css({"background-color": "#34495e", "color" : "white", "border-radius": "50px",
                          "text-align" : "center"});
       
        $(".accordion").css({"background-color": "#2c3e50", "outline": "none", "color" : "white",
    							"border": "0px", "border-radius": "50px"});
        
        $(".card-title").css({"color": "white"})
        $("#status").css({"color": "white"})
        $(".card-body").css({"background-color": "#7f8c8d", "border-radius":"50px"})
        $(".card").css({"background-color": "#7f8c8d", "border-radius":"50px"})
    });

    $(".accordion").click(function(){
        var acc = document.getElementsByClassName("accordion");
        var i;

        for (i = 0; i < acc.length; i++) {
          acc[i].addEventListener("click", function() {
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.maxHeight){
              panel.style.maxHeight = null;
            } else {
              panel.style.maxHeight = panel.scrollHeight + "px";
            } 
          });
        }
    });
});


