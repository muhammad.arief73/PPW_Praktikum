from django.shortcuts import render
from django.http import HttpResponseRedirect, JsonResponse
from django.contrib.auth.models import User
from .forms import Status_Form
from .forms import Regist_form
from .models import Status
from .models import Regist
import json
import requests
from django.contrib.auth import logout as logout_user


# Create your views here.


def content(request):
	all_status = Status.objects.all()
	form = Status_Form
	return render(request, 'landing_page.html', {'Status': all_status, 'form': form})

def profil(request):
	return render(request, 'profil.html', {})

def add_status(request):
	form = Status_Form(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		status = request.POST.get('status',False)
		status = Status(status=status)
		status.save()
		html ='landing_page.html'
		return HttpResponseRedirect('/lab-6/')
	else:
		return HttpResponseRedirect('/lab-6/')
	
	
def delete_status(request):
	clear_status = Status.objects.all()
	clear_status.delete()
	html ='landing_page.html'
	return HttpResponseRedirect('/lab-6/')



# def getJSON(request):
# 	jsonbooks = requests.get('https://www.googleapis.com/books/v1/volumes?q=quilting').json()
# 	return JsonResponse(jsonbooks)

# Views buat lab 9
def getJSON(request):
	text = request.GET.get('cari','quilting')
	jsonbooks = requests.get('https://www.googleapis.com/books/v1/volumes?q='+text).json()
	return JsonResponse(jsonbooks)

def books(request):
	return render(request, 'daftar_books.html', {})
# Lab 10
def reg(request):
	html = "lab_10.html"
	return render(request, html)

def validateEmail(request):
	isExist = False
	if 'email' in request.POST:
		count = Regist.objects.filter(email = request.POST['email']).count()
		if count > 0:
			isExist = True
	return JsonResponse({"email_is_exist": isExist}, content_type = 'application/json')
	

def add_form(request):
	if request.method == 'POST':
		name = request.POST['nama']
		email = request.POST['email']
		password = request.POST['password']

		form_regist = Regist.objects.create(
            name = name,
            email = email,
            password = password,
        )     

		form_regist.save()
		
		return HttpResponse('')
	else:
		return HttpResponseRedirect('lab-6/register/')

def logIn(request):
	return render(request, 'startPageBooks.html')

def logout(request):
    logout_user(request)
    return HttpResponseRedirect('/lab-6/signIn/')
