from django.contrib import admin
from .models import Status, Regist

# Register your models here.
admin.site.register(Status)
admin.site.register(Regist)