from django.urls import path
from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from lab6 import views


#url for app
urlpatterns = [
		# lab 6
        path('lab-6/', views.content, name='content'),
        path('profile/', views.profil, name='profil'),
        url(r'^add_status/$', views.add_status, name = 'add_status'),
        url(r'^delete_status/$', views.delete_status, name = 'delete_status'),
        # Lab 8
        path('signIn/', views.logIn, name='login'),
        path('buku/', views.books, name='books'),
        path('logout/', views.logout, name = 'logout'),
        # Lab 9
        path('list_books/', views.getJSON, name = 'getJSON'),
        # lab 10
        path('register/', views.reg, name = 'reg'),
        path('add_form/', views.add_form, name = 'add_form'),
        path('validate/', views.validateEmail, name = 'validateEmail'),

        #lab 11
        url(r'^admin/', admin.site.urls),
    	url(r'^auth/', include('social_django.urls', namespace='social')),  # <- Here
    
    	



        # base
        path('', views.content, name='content'),
        
        

        


]

urlpatterns += staticfiles_urlpatterns()
