from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import Jadwal


# Create your views here.

nama = 'Muhammad Arief Darmawan'
address = 'Jalan Seruni 3, No 21'
birth = '28 January 1999'
phone = '+6282112959500'
email = 'darmawanarief71@gmail.com'

qoute ='Quotes'
register = 'Register'

def index(request):
    response = {'name': nama, 'address' : address, 'birth' : birth,
                'phone' : phone, 'email' : email}
    return render(request, 'index.html', response)

def quote(request):
    response = {'name': nama,'quote': quote}
    return render(request, 'My Portofolio.html', response)

def register(request):
    response = {'name': nama,'register': register}
    return render(request, 'Register.html', response)

def regsuccsess(request):
    return render(request, 'reg_succsess.html', response)

def jadwal(request):
	all_jadwal = Jadwal.objects.all()

	return render(request,'jadwal.html', {'Jadwal': all_jadwal})

def add_jadwal(request):
	#form = Jadwal_form(request.POST or None)
	##if(request.method == 'POST' and form.is_valid()):
	kegiatan = request.POST.get("activiti", False)
	hari = request.POST.get("day", False)
	tanggal = request.POST.get("date", False)
	jam = request.POST.get("time", False)
	tempat = request.POST.get("place", False)
	kategori = request.POST.get("kategori", False)

	jadwal = Jadwal(
		kegiatan = kegiatan, 
		hari = hari,
		tanggal = tanggal,
		jam =  jam,
		tempat = tempat,
		kategori_m =  kategori
		)

	jadwal.save()
	html ='jadwal.html'
	return render(request, html)
	
def delete(request):
	clear_jadwal = Jadwal.objects.all()
	clear_jadwal.delete()
	html ='jadwal.html'
	return render(request, html)





