# Generated by Django 2.1.1 on 2018-10-03 11:09

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('biodata', '0002_auto_20181003_1805'),
    ]

    operations = [
        migrations.RenameField(
            model_name='jadwal',
            old_name='hari_m',
            new_name='hari',
        ),
        migrations.RenameField(
            model_name='jadwal',
            old_name='jam_m',
            new_name='jam',
        ),
        migrations.RenameField(
            model_name='jadwal',
            old_name='kegiatan_m',
            new_name='kegiatan',
        ),
        migrations.RenameField(
            model_name='jadwal',
            old_name='tanggal_m',
            new_name='tanggal',
        ),
        migrations.RenameField(
            model_name='jadwal',
            old_name='tempat_m',
            new_name='tempat',
        ),
    ]
