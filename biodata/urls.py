from django.urls import path
from django.conf.urls import url
from biodata import views


#url for app
urlpatterns = [
	path('home/', views.index, name='index'),
	path('quote/', views.quote, name='quote'),
	path('register/', views.register, name='register'),
	path('reg_Complete/', views.regsuccsess, name='regsuccsess'),
	path('jadwal/', views.jadwal, name='jadwal'),
    path('', views.index, name='index'),
    url(r'^add_jadwal/$', views.add_jadwal, name = 'add_jadwal'),
    url(r'^delete/$', views.delete, name = 'delete'),
   
]
