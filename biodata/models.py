from django.db import models

# Create your models here.
class Jadwal(models.Model):
	kegiatan = models.CharField(max_length=200)
	hari = models.CharField(max_length=200)
	tanggal = models.CharField(max_length=200)
	jam = models.CharField(max_length=200)
	tempat = models.CharField(max_length=200)
	kategori_m = models.CharField(max_length=200)
